  <?php include('../includes/header_common.phtml');?>


  <section class="bg-headline-wrap">
    <div class="inner-wrap">
      <h1 class="section-headline white">MAD MANSION 1: PROYECTO GIBEON</h1>
    </div>
  </section>

  <section class="content-wrap white">
    <div class="site-container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 margin20bottom">
          <h6 class="small-headline">Video</h6>
          <a href="//www.youtube.com/embed/qg_b2fdJZFE?autoplay=1" class="video-player-box video-link fancybox.iframe">
            <img src="../img/mad1/video.png" alt="Youtube" />
          </a>
          <h6 class="small-headline">Gallery</h6>
          <ul class="gallery-box">
            <li><a href="../img/mad1/1.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad1/1.jpg?v=1" alt="Img 1" /></a></li>
            <li><a href="../img/mad1/2.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad1/2.jpg?v=1" alt="Img 2" /></a></li>
            <li><a href="../img/mad1/3.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad1/3.jpg?v=1" alt="Img 3" /></a></li>
            <li><a href="../img/mad1/4.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad1/4.jpg?v=1" alt="Img 4" /></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-6 margin20bottom">
          <h6 class="small-headline">SINOPSIS</h6>
          <p class="text">
            La familia Crowell esconde un oscuro secreto en su mansión. Una noche cualquiera se os ocurre la idea de acercaros a saber cuál es. Nada más llegar al jardín de esa extraña casa, la puerta se cierra y quedáis atrapados. Disponéis de 1 hora para descubrir el misterio y escapar antes de que la familia Crowell vuelva y os pillen merodeando por su mansión. ¿Lo conseguiréis?
          </p>

<div id="widget-iframe-content" data-app-url="https://www.ticketself.com/" data-event-url="https://www.ticketself.com/event/35d93c70?widget=1&isgift=1&widconf=23"> Compra aquí tus entradas</div> <script type="text/javascript" src="https://www.ticketself.com/assets/default/js/widget-iframe.js?v=7"></script>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <div class="room-info">
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-timer"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">DURACION</h6>
                <p class="text big">60 mins.</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-user"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">Participantes</h6>
                <p class="text big"> 2-5 personas</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-cash"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline"> Precio</h6>
                <p class="text big">65 &euro;</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

   <?php include('../includes/footer.phtml');?>