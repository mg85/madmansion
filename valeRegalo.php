  <?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container">
      <h2 class="section-headline">REGALA MAD MANSION</h2>
      <h5 class="subheadline margin40bottom">¿Estás pensando en qué regalar?</h5>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
        <strong class="small-headline">Sala 1</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img1.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 2-5 participantes</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 60 mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 65 € (por grupo)</span>
                </p>
                <a href="/valeregalo/proyecto-gibeon.php">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">MAD MANSION 1: PROYECTO GIBEON</h3>
              <p class="text">
                La familia Crowell esconde un oscuro secreto en su mansión. Una noche cualquiera se os ocurre la idea de acercaros a saber cuál es. Nada más llegar al jardín de esa extraña  casa, la puerta se cierra y quedáis atrapados. Disponéis de 1 hora para descubrir el misterio y escapar ...
              </p>
              <a class="yellow-btn" href="/valeregalo/proyecto-gibeon.php">Regalar</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
        <strong class="small-headline">Sala 2</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img2.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 2-5 participantes</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 60 mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 65 € (por grupo)</span>
                </p>
                <a href="valeregalo/una-nueva-especie.php">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">MAD MANSION 2: UNA NUEVA ESPECIE</h3>
              <p class="text">
                El Dr. Crowell os ha atrapado en su mansión y quiere llevar  a cabo el "Proyecto Gibeon" con vosotros. Disponéis de 1 hora para sabotearle el plan y escapar antes de que sea demasiado tarde.  Solo usando vuestro ingenio y siendo más astutos que él, podréis lograrlo. ¡Suerte! 
              </p>
              <a class="yellow-btn" href="/valeregalo/una-nueva-especie.php">Regalar</a>
            </div>
          </div>
        </div>
 
      </div>
    </div>
  </section>

<?php include('includes/footer.phtml');?>