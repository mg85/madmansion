  <?php include('includes/header.phtml');?>
  <div class="intro-wrap">
    <div class="bg-wrapper">
      <div class="poster"></div>
      <video muted autoplay="autoplay" loop preload="auto" id="bg_video">
        <source src="video/mmbg.mp4" type="video/mp4">
        <source src="video/mmbg.webm" type="video/webm">       
      </video>
    </div>
    <!-- /.bg-wrapper -->
    <div class="intro-inner-wrap">
      <div class="intro-box">
        <div class="img-wrap desktop-hidden">
          <img src="img/logo-2x.png" alt="Escape" />
        </div>
        <h1 class="intro-headline desktop-hidden">Valorado #1 mejor juego en tripAdvisor</h1>
        <div class="box award-box-wrap">
          <div class="box margin40top">
            <a href="juegos.php" class="white-btn intro">Reservar</a>
          </div>
          <div id="award_box" class="award-box">
            <img src="img/medal.png" alt="Award" class="medal" />
            <div id="TA_excellent511" class="TA_excellent" style="margin-left: 48px;"><ul id="tKj75jD4" class="TA_links TAaPNtv" >
              <li id="VYV6J2zQe" class="mxxmksHjLn"><a target="_blank" href="http://www.tripadvisor.es/">
                <img src="http://e2.tacdn.com/img2/widget/tripadvisor_logo_115x18.gif" alt="TripAdvisor" class="widEXCIMG" id="CDSWIDEXCLOGO"/>
              </a></li></ul></div><script src="http://www.jscache.com/wejs?wtype=excellent&uniq=511&locationId=6721748&lang=es&display_version=2"></script>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro-wrap -->

  <!--
    <div class="award-medal-wrap">
      <div class="site-container">
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-lg-4 a-right">
            <div class="inner-wrap">
              <img src="img/medal.png" alt="Medal" class="medal-img" />
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-lg-4">
            <div class="inner-wrap">
              <strong class="award-medal-title">
                ¡Bravo!<br>
                Mad Mansion ha sido calificado<br/> como "Excelente" por 829 viajeros
              </strong>
            </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-lg-4">
            <div class="inner-wrap">
              <a href="https://www.tripadvisor.es/Attraction_Review-g187454-d6721748-Reviews-Mad_Mansion-Bilbao_Province_of_Vizcaya_Basque_Country.html" class="read-more-btn">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
     /.award-medal-wrap -->

  <section class="content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">¿Podrás escapar?</h2>
      <h5 class="subheadline">Tienes 60 minutos para escapar de Mad Mansion</h5>
      <p class="text big text-center">
        Los juegos de Mad Mansion giran en torno a la extraña familia Crowell y lo que sucede en su mansión. No son de miedo, son juegos diferentes e independientes entre sí, de dicultad similar unidos por la misma historia. Se necesita cooperación, trabajar en equipo y usar vuestro ingenio para completarlos. ¿Aceptáis el reto?
      </p>
    </div>
    <div class="site-container">
      <div class="box margin40top">
        <div class="features-list row">
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-mouse"></i>
          </span>
            <strong class="headline">Haz tu reserva online</strong>
            <p class="text">
              Podrás elegir la fecha y la hora en la que intentaréis completar el reto
            </p>
          </div>
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-unlock"></i>
          </span>
            <strong class="headline">Liberate en 60 minutos</strong>
            <p class="text">
              Una cuenta atrás de 60 minutos comenzará a desconectar el tiempo que os queda para poder salir
            </p>
          </div>
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-light"></i>
          </span>
            <strong class="headline">Resuelve diferentes puzzles</strong>
            <p class="text">
              Para escapar deberéis encontrar y superar diferentes puzzles que os llevarán a la salida
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content-wrap -->

  <div class="content-wrap who-can-play">
    <div class="site-container">
      <h2 class="section-headline white">¿Quien puede jugar?</h2>
      <h5 class="subheadline">Los juegos de Mad Mansion están diseñados para todos los jugadores</h5>
      <div class="box margin40top">
        <div class="small-features-list row">
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-smile"></i>
          </span>
            <strong class="headline">Familia y amigos</strong>
            <p class="text">
              Si quieres vivir una experiencia inolvidable con tu familia y amigos, Mad Mansion es lo que estás buscando.
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-plane"></i>
          </span>
            <strong class="headline">Turistas</strong>
            <p class="text">
              No pierdas la oportunidad de llevarte un recuerdo imborrable, Mad Mansion es la opción #1 elegida por los turistas en TripAdvisor
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-joy"></i>
          </span>
            <strong class="headline">Jugadores</strong>
            <p class="text">
              Vive la aventura de un videojuego en vivo y en directo en el que tu serás el protagonista
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-diamond"></i>
          </span>
            <strong class="headline">Equipos de trabajo</strong>
            <p class="text">
              Demuestra que trabajar en equipo es una de tus cualidades y acepta el reto con tus compañeros de trabajo
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.content-wrap -->

  <div class="content-wrap white">
    <div class="site-container">
      <div class="box">
        <div class="stats-list row">
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">3</span>
            <span class="stats-name">Juegos</span>
          <span class="icon-box">
            <i class="pe-7s-diamond"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">175</span>
            <span class="stats-name">Puzles</span>
          <span class="icon-box">
            <i class="pe-7s-lock"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">18,132</span>
            <span class="stats-name">Jugadores</span>
          <span class="icon-box">
            <i class="pe-7s-users"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">60</span>
            <span class="stats-name">Minutos</span>
          <span class="icon-box">
            <i class="pe-7s-timer"></i>
          </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.content-wrap -->

  <div class="book-now-wrap">
    <div class="site-container">
      <div class="row">
        <div class="col-sm-8">
          <strong class="book-now-title">
            No esperes más<br> y haz tu resera online
          </strong>
        </div>
        <div class="col-sm-4">
          <a href="#" class="book-now-btn">Reservar</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /.book-now-wrap -->

  <div class="content-wrap white">
    <div class="site-container">
      <div class="row">
        <div class="col-sm-4 text-center">
          <div class="box">
            <img src="img/tripadvisor.png" alt="Tripadvisor" class="tripadvisor-img" />
          </div>
          <h4 class="text-center testimonials-headline">
            Valorado #1 mejor juego <br/> en tripadvisor
          </h4>
        </div>
        <div class="col-sm-8">
          <div class="testimonials-slider-wrap">
            <ul class="testimonials-slider">
              <li>
                <h5 class="headline">“Súper entretenida casa de escape”</h5>
                <p class="text big">
                  Lo pasamos genial!! Es todo un reto y esta genial ambientado. Nos trataron estupendamente además. Nos hemos quedado con las ganas de los demás juegos. Recomendadisimo.
                </p>
                <span class="author">Ithael</span>
              </li>
              <li>
                <h5 class="headline">“Emocionante desde el primer minuto”</h5>
                <p class="text big">
                  Mis amigos y yo hicimos el Juego 2 y fue una experiencia genial!! He jugado a varias escape room en diferentes ciudades y sin duda está entre las mejores, entre las que he recomendado y recomendaré a mis amigos. Estuvimos en tensión durante toda la sesión. Adrenalina pura!! Iremos a hacer el Juego 1 lo antes posible :)
                </p>
                <span class="author">Enara P</span>
              </li>
              <li>
                <h5 class="headline">“La mejor sala de escape del País Vasco”</h5>
                <p class="text big">
                  Pioneros y con 3 salas espectaculares. Realizamos la 1a hace un año y ahora volvimos a por la 2da. No defraudan. Sin duda las mejores en las que hemos estado.
                </p>
                <span class="author">IGF5</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.testimonials-wrap -->
<?php include('includes/footer.phtml');?>