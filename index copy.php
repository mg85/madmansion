  <?php include('includes/header.phtml');?>
  <div class="intro-wrap">
    <div class="intro-inner-wrap">
      <div class="intro-box">
        <div class="img-wrap">
          <img src="img/logo-2x.png" alt="Escape" />
        </div>
        <h1 class="intro-headline">Valorado juego número 1 en diversión y juegos</h1>
        <div class="box text-center margin80top">
          <a href="juegos.php" class="white-btn">Reservar</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro-wrap -->
  
  <div class="award-medal-wrap">
    <div class="site-container">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-lg-4 a-right">
          <div class="inner-wrap">
            <img src="img/medal.png" alt="Medal" class="medal-img" />
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <div class="inner-wrap">
            <strong class="award-medal-title">
              ¡Bravo!<br>
              Mad Mansion ha sido calificado<br/> como "Excelente" por 829 viajeros
            </strong>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-lg-4">
          <div class="inner-wrap">
            <a href="https://www.tripadvisor.es/Attraction_Review-g187454-d6721748-Reviews-Mad_Mansion-Bilbao_Province_of_Vizcaya_Basque_Country.html" class="read-more-btn">Read More</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.award-medal-wrap -->

  <section class="content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">¿Podrás escapar?</h2>
      <h5 class="subheadline">Tienes 60 minutos para escapar de Mad Mansion</h5>
      <p class="text big text-center">
        Los juegos de Mad Mansion giran en torno a la extraña familia Crowell y lo que sucede en su mansión. No son de miedo, son juegos diferentes e independientes entre sí, de dicultad similar unidos por la misma historia. Se necesita cooperación, trabajar en equipo y usar vuestro ingenio para completarlos. ¿Aceptáis el reto?
      </p>
    </div>
    <div class="site-container">
      <div class="box margin40top">
        <div class="features-list row">
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-mouse"></i>
          </span>
            <strong class="headline">Haz tu reserva online</strong>
            <p class="text">
              Podrás elegir la fecha y la hora en la que intentaréis completar el reto
            </p>
          </div>
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-unlock"></i>
          </span>
            <strong class="headline">Liberate en 60 minutos</strong>
            <p class="text">
              Una cuenta atrás de 60 minutos comenzará a desconectar el tiempo que os queda para poder salir
            </p>
          </div>
          <div class="features-box col-sm-4">
          <span class="icon-box">
            <i class="pe-7s-light"></i>
          </span>
            <strong class="headline">Resuelve diferentes puzles</strong>
            <p class="text">
              Para escapar deberéis encontrar y superar diferentes puzles que os llevarán a la salida
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content-wrap -->

  <div class="content-wrap who-can-play">
    <div class="site-container">
      <h2 class="section-headline white">¿Quien puede jugar?</h2>
      <h5 class="subheadline">Los juegos de Mad Mansion están diseñados para todos los jugadores</h5>
      <div class="box margin40top">
        <div class="small-features-list row">
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-smile"></i>
          </span>
            <strong class="headline">Familia y amigos</strong>
            <p class="text">
              Si quieres vivir una experiencia inolvidable con tu familia y amigos, Mad Mansion es lo que estás buscando.
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-plane"></i>
          </span>
            <strong class="headline">Turistas</strong>
            <p class="text">
              No pierdas la oportunidad de llevarte un recuerdo imborrable, Mad Mansion es la opción #1 elegida por los turistas en TripAdvisor
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-joy"></i>
          </span>
            <strong class="headline">Jugadores</strong>
            <p class="text">
              Vive la aventura de un videojuego en vivo y en directo en el que tu serás el protagonista
            </p>
          </div>
          <div class="small-feature col-sm-6 col-sm-3">
          <span class="icon-box">
            <i class="pe-7s-diamond"></i>
          </span>
            <strong class="headline">Equipos de trabajo</strong>
            <p class="text">
              Demuestra que trabajar en equipo es una de tus cualidades y acepta el reto con tus compañeros de trabajo
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.content-wrap -->

  <div class="content-wrap white">
    <div class="site-container">
      <div class="box">
        <div class="stats-list row">
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">3</span>
            <span class="stats-name">Juegos</span>
          <span class="icon-box">
            <i class="pe-7s-diamond"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">175</span>
            <span class="stats-name">Puzles</span>
          <span class="icon-box">
            <i class="pe-7s-lock"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">18,132</span>
            <span class="stats-name">Jugadores</span>
          <span class="icon-box">
            <i class="pe-7s-users"></i>
          </span>
          </div>
          <div class="stats-box col-xs-12 col-sm-6 col-md-3">
            <span class="stats-number">60</span>
            <span class="stats-name">Minutos</span>
          <span class="icon-box">
            <i class="pe-7s-timer"></i>
          </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.content-wrap -->

  <div class="book-now-wrap">
    <div class="site-container">
      <div class="row">
        <div class="col-sm-8">
          <strong class="book-now-title">
            No esperes más<br> y haz tu resera online
          </strong>
        </div>
        <div class="col-sm-4">
          <a href="#" class="book-now-btn">Reservar</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /.book-now-wrap -->

  <div class="content-wrap white">
    <div class="site-container">
      <div class="row">
        <div class="col-sm-4 text-center">
          <div class="box">
            <img src="img/tripadvisor.png" alt="Tripadvisor" class="tripadvisor-img" />
          </div>
          <h4 class="text-center testimonials-headline">
            The #1 rated Fun and Games<br/> attraction in Planet
          </h4>
        </div>
        <div class="col-sm-8">
          <div class="testimonials-slider-wrap">
            <ul class="testimonials-slider">
              <li>
                <h5 class="headline">It was a great experience..</h5>
                <p class="text big">
                  Curabitur pellentesque mauris sit amet dui viverra,
                  quis sodales lorem suscipit. Praesent pulvinar eu velit eu tempor.
                  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                  officia deserunt mollit anim id est laborum.
                </p>
                <span class="author">John Doe</span>
              </li>
              <li>
                <h5 class="headline">Super Escape Room</h5>
                <p class="text big">
                  Curabitur pellentesque mauris sit amet dui viverra,
                  quis sodales lorem suscipit. Praesent pulvinar eu velit eu tempor.
                  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                  officia deserunt mollit anim id est laborum.
                </p>
                <span class="author">Andrix Brown</span>
              </li>
              <li>
                <h5 class="headline">Perfect for children and parents </h5>
                <p class="text big">
                  Curabitur pellentesque mauris sit amet dui viverra,
                  quis sodales lorem suscipit. Praesent pulvinar eu velit eu tempor.
                  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                  officia deserunt mollit anim id est laborum.
                </p>
                <span class="author">Lily Doe</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.testimonials-wrap -->
<?php include('includes/footer.phtml');?>