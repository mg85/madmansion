  <?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">Franquicias</h2>
      <h5 class="subheadline margin40bottom">FRANQUICIAS ASOCIADAS A NUESTROS SERVICIOS</h5>
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <img src="img/samples/franchise_1.jpg" alt="Franquicias" class="game-photo" />
          </div>
        </div>
      </div>
      <p class="text">
        Mad Mansion ofrece la opción de llevar esta idea de negocio a tu ciudad. Una oportunidad de exportarlo con su marca y todo el proceso de trabajo que hay detrás.</br>
 
        Disponemos de distintos juegos de gran calidad, preparados para poder ponerlos en funcionamiento en poco tiempo.
 
        
      </p>
      <p class="text">
        Intersados contactar con nosotros:</br></br>
        Teléfono: 622034781</br>
        Email: info@madmansion.es </br>
        Web: http://www.madmansion.es</br>
        C/ Huertas de la villa, 16. (Uriortu Kalea, 16),</br>
        Bilbao 48007, Vizcaya.
      </p>
      <p class="text">
        
      </p>
    </div>
  </section>

    <?php include('includes/footer.phtml');?>