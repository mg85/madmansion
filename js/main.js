$(function() {
  // Testimonials slider
  if ($('.testimonials-slider').length) {
    $('.testimonials-slider').bxSlider({
      minSlides: 1,
      maxSlides: 1,
      controls: false,
      auto: true,
      speed: 800
    });
  }

  /* Fancybox Image modal */
  if ($('.fancybox').length) {
    $('.fancybox, .video-link').fancybox({
      padding: 5,
      maxWidth	: 800,
      maxHeight	: 600,
      fitToView	: false,
      width		: '70%',
      height		: '70%',
      autoSize	: false,
      closeClick	: false,
      openEffect	: 'none',
      closeEffect	: 'none'
    });
  }

  $('html').click(function() {
    $(".lang-wrap").removeClass("in");
  });

  $(".lang-toggle").on("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest(".lang-wrap").toggleClass("in");
  });

  $(".topnav-toggle").on("click", function(e) {
    e.preventDefault();
    $(this).toggleClass("in");
    $(".topnav-wrap").toggleClass("in");
  });

  $(window).on("load", function() {
    $("#award_box").addClass("visible");
  });

  // top nav fixed on scroll
  $(window).on('scroll load', function () {
    var topPos = $(this).scrollTop();
    if (topPos > 0) {
      $('.main-header').addClass('fixed');
      return false;
    }
    $('.main-header').removeClass('fixed');
  });
});