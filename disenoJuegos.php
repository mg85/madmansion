<?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">Diseñamos juegos</h2>
      <h5 class="subheadline margin40bottom">DISEÑO PERSONALIZADO DE JUEGOS</h5>

      <p class="text a-center">
        ¿Tienes una idea pero no sabes cómo llevarla a cabo? </br>
        MAD MANSION ofrece  ahora un asesoramiento personalizado.
      </p>
      <p class="text a-center">
        Diseñamos juegos a medida, en base a necesidades de cada cliente.</br>
        Estudiando las distintas posibilidades siempre con el objetivo final de que sean juegos de calidad.</br>
        Contacta con nosotros para más información:</br>
      </p>
      <p class="text a-center">
        Teléfono: 622034781</br>
        Email: <a href="mailto:
&#105;&#110;&#102;&#111;&#064;&#109;&#097;&#100;&#109;&#097;&#110;&#115;&#105;&#111;&#110;&#046;&#101;&#115;&#032;">
&#105;&#110;&#102;&#111;&#064;&#109;&#097;&#100;&#109;&#097;&#110;&#115;&#105;&#111;&#110;&#046;&#101;&#115;&#032;
</a></br>
        C/ Huertas de la villa, 16. (Uriortu Kalea, 16),</br>
        Bilbao 48007, Vizcaya.
      </p>
    </div>
    <div class="site-container">
      <div class="row margin40top">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img1.jpg" class="fancybox" rel="group1" title="Photo Description #1">
            <img src="img/game_img1.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img2.jpg" class="fancybox" rel="group1" title="Photo Description #2">
            <img src="img/game_img2.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img3.jpg" class="fancybox" rel="group1" title="Photo Description #3">
            <img src="img/game_img3.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img4.jpg" class="fancybox" rel="group1" title="Photo Description #4">
            <img src="img/game_img4.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img5.jpg" class="fancybox" rel="group1" title="Photo Description #5">
            <img src="img/game_img5.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img6.jpg" class="fancybox" rel="group1" title="Photo Description #6">
            <img src="img/game_img6.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img4.jpg" class="fancybox" rel="group1" title="Photo Description #7">
            <img src="img/game_img1.jpg" alt="Game 4" class="game-photo" />
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="img/game_img2.jpg" class="fancybox" rel="group1" title="Photo Description #8">
            <img src="img/game_img2.jpg" alt="Game 1" class="game-photo" />
          </a>
        </div>
      </div>
    </div>
  </section>

 <?php include('includes/footer.phtml');?>