  <?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container">
      <h2 class="section-headline">Vale Regalo</h2>
      <h5 class="subheadline margin40bottom">Vivamus eget sapien vel nibh maximus placerat</h5>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img1.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">Gibeon Project</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img2.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">A New Species</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img3.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">The secret of the templarie</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img4.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">Dr. Crowells Rescue</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img5.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">LOST</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img6.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 3-7 participants</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 45mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 47%</span>
                </p>
                <a href="#">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">Sherlock Holmes</h3>
              <p class="text">
                Integer in viverra massa, quis ultrices nibh. Phasellus fringilla ipsum sed turpis
                accumsan, vel eleifend turpis tincidunt. Praesent vulputate mat
              </p>
              <a class="yellow-btn" href="one-game.php">Give a Present</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    <?php include('includes/footer.phtml');?>