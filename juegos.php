  <?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container">
      <h2 class="section-headline">Juegos</h2>
      <h5 class="subheadline margin40bottom">Elige entre nuestros juegos</h5>
      <div class="box">
        <h3 class="box-headline big">BILBAO</h3>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <strong class="small-headline">Sala 1</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img1.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 2-5 participantes</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 60 mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 65 € (Grupo)</span>
                </p>
                <a href="juegos/proyecto-gibeon.php">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">MAD MANSION 1: PROYECTO GIBEON</h3>
              <p class="text">
                La familia Crowell esconde un oscuro secreto en su mansión. Una noche cualquiera se os ocurre la idea de acercaros a saber cuál es. Nada más llegar al jardín de esa extraña  casa, la puerta se cierra y quedáis atrapados. Disponéis de 1 hora para descubrir el misterio y escapar  ...
              </p>
              <a class="yellow-btn" href="juegos/proyecto-gibeon.php">Ir a reservas</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
        <strong class="small-headline">Sala 2</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/game_img2.jpg" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> 2-5 participantes</span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> 60 mins.</span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> 65 € (Grupo)</span>
                </p>
                <a href="juegos/una-nueva-especie.php">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">MAD MANSION 2: UNA NUEVA ESPECIE</h3>
              <p class="text">
                El Dr. Crowell os ha atrapado en su mansión y quiere llevar  a cabo el "Proyecto Gibeon" con vosotros. Disponéis de 1 hora para sabotearle el plan y escapar antes de que sea demasiado tarde.  Solo usando vuestro ingenio y siendo más astutos que él, podréis lograrlo. ¡Suerte! 
              </p>
              <a class="yellow-btn" href="juegos/una-nueva-especie.php">Ir a reservas</a>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
        <strong class="small-headline">2 Salas</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/dinorising_2.jpg?v=<?php echo time();?>" alt="">
              <figcaption>
                <p>
                  <span><i class="pe pe-lg pe-7s-user"></i> </span>
                  <span><i class="pe pe-lg pe-7s-timer"></i> </span>
                  <span><i class="pe pe-lg pe-7s-upload"></i> </span>
                </p>
                <a href="http://www.dinorising.com/" target="_blank">Book Now</a>
              </figcaption>
            </figure>

            <div class="caption">
              <h3 class="headline">DINO RISING</h3>
              <p class="text">
                El complejo futurista de “OMEGA Corporation” situado en un planeta lejano, solicita ayuda. Algo ha fallado y necesitan personal para ir a investigar y saber que es.<br> Dos grupos, Equipo Rojo y Equipo Azul, deciden acudir a la llamada. Lo que se encuentran al llegar es muy distinto a lo que se esperaban. Seréis capaces de descubrir lo que está sucediendo?
              </p>
              <a class="yellow-btn" href="http://www.dinorising.com/">Ir a reservas</a>
            </div>
          </div>
        </div>
      </div>
   <!--   
      <div class="row margin20top">
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="box">
            <h3 class="box-headline big">PRÓXIMAMENTE</h3>
          </div>
          <strong class="small-headline">&nbsp;</strong>
          <div class="item">
            <figure class="effect-apollo">
              <img class="img-responsive" src="img/invasion.jpg?v=<?php echo time();?>" alt="">
              
            </figure>

            <div class="caption">
              <h3 class="headline">INVASION</h3>
              <p class="text">
                Año 2045. El mundo está siendo invadido por grupos de alienígenas. Como equipo de elite, sois la única esperanza. En grupos de 5 tenéis que ser los más rápidos en salvar el mayor número de Países antes de que sea demasiado tarde.
              </p>
              <a class="yellow-btn" href="#">Próximamente</a>
            </div>
          </div>
        </div> -->
        <div class="col-xs-12 col-sm-6 col-md-4 games-box">
          <div class="box">
            <h3 class="box-headline big">PAMPLONA</h3>
          </div>
          <strong class="small-headline">Sala 1</strong>
          <div class="item">
            <figure class="effect-apollo">
              <a href="https://overtimepamplona.es/" target="_blank"><img class="img-responsive" src="img/overtime.jpg?v=<?php echo time();?>" alt=""></a>
              
            </figure>

            <div class="caption">
              <h3 class="headline">OVERTIME: Misión 1: 100 AÑOS DE PERDÓN.</h3>
              <p class="text">
                Ha habido una filtración importante de que el líder de la república de Shadow Island, Raúl Menéndez, está tratando de construir una nueva arma nuclear. Ha depositado los planos en uno de los bancos de la red “General Bank” controlados por él. Como equipo “Ghost”, (expertos en infiltración) vuestra misión es: Buscar la manera de entrar en el banco, localizar los planos y sustraerlos sin dejar rastro. De esta manera imputaremos a Menéndez  y será puesto entre rejas de una vez por todas.
              </p>
              <a class="yellow-btn" target="_blank" href="https://overtimepamplona.es/">Ir a reservas</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('includes/footer.phtml');?>