  <?php include('includes/header_common.phtml');?>


  <section class="bg-headline-wrap">
    <div class="inner-wrap">
      <h1 class="section-headline white">THE SECRET OF THE TEMPLARIE</h1>
    </div>
  </section>

  <section class="content-wrap white">
    <div class="site-container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 margin20bottom">
          <h6 class="small-headline">Video</h6>
          <a href="https://www.youtube.com/embed/qg_b2fdJZFE?autoplay=1" class="video-player-box video-link fancybox.iframe">
            <img src="img/youtube_img.jpg" alt="Youtube" />
          </a>
          <h6 class="small-headline">Gallery</h6>
          <ul class="gallery-box">
            <li><a href="img/game_img1.jpg" class="fancybox" data-fancybox-group="gallery"><img src="img/game_img1.jpg" alt="Img 1" /></a></li>
            <li><a href="img/game_img2.jpg" class="fancybox" data-fancybox-group="gallery"><img src="img/game_img2.jpg" alt="Img 2" /></a></li>
            <li><a href="img/game_img3.jpg" class="fancybox" data-fancybox-group="gallery"><img src="img/game_img3.jpg" alt="Img 3" /></a></li>
            <li><a href="img/game_img4.jpg" class="fancybox" data-fancybox-group="gallery"><img src="img/game_img4.jpg" alt="Img 4" /></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-6 margin20bottom">
          <h6 class="small-headline">OVERVIEW</h6>
          <p class="text">
            MAD TOURNÉ It is a cooperative adventure game.<br/>
            Players embody some learners detectives hired by Vincent Warren, an adventurer
            and treasure hunter conocidísimo. Vicent, wants to create a detective agency.
            To find out if the players are worthy of belonging to it, it creates a series
            of trials that players must complete for show
          </p>
          <p class="text">
            Will you be able to convince Vincent and join his team of detectives? Only
            the boldest have that privilege. Welcome to Mad Tourné!
          </p>
          <div class="widget-wrap">
            <iframe width="100%" height="800" src="https://www.ticketself.com/embed/f3021e0e?wid=3" marginheight="0" align="top" scrolling="yes" frameborder="0" hspace="0" vspace="0" allowfullscreen></iframe> <a href="http://eventises.com" title="Comprar y vender entradas"><img src="https://www.ticketself.com/static/img/eventisespowered.png" border="0"></a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <div class="room-info">
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-timer"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">DURATION</h6>
                <p class="text big">45mins.</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-user"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">GROUP SIZE</h6>
                <p class="text big"> 3-7 participants</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-cash"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline"> Price</h6>
                <p class="text big">60 &euro;</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

   <?php include('includes/footer.phtml');?>
