  <?php include('includes/header_common.phtml');?>

  <div class="faq-wrap">
    <div class="site-container">
      <h2 class="section-headline white">FAQ</h2>
      <h5 class="subheadline margin40bottom">Preguntas frecuentes</h5>
      <div class="collapse-wrap">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_1" aria-expanded="true">
                 ¿A PARTIR DE QUE EDAD SE PUEDE ENTRAR EN MAD MANSION?

                </a>
              </h4>
            </div>
            <div id="panel_1" class="panel-collapse collapse in" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  MAD MANSION es apto para todos lo públicos. Pero debido a la complejidad de algunas pruebas y la edad mental adquirida, sólo los mayores de 10 años pueden participar en nuestra gran aventura, teniendo en cuenta que los menores de 16 años deberán de tener como mínimo un adulto en su equipo. 
                  No existe ningún límite máximo de edad, cualquiera puede disfrutar de MAD MANSION por igual. ¿A que esperáis para vivir una experiencia nueva y emocionante? 
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_2" aria-expanded="false">
                  ¿PUEDO DISFRUTAR MAD MANSION SI SUFRO CLAUSTROFOBIA?
                </a>
              </h4>
            </div>
            <div id="panel_2" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  Como aclaramos en la primera pregunta no se trata de un juego de miedo ni claustrofóbico, por lo que no vemos ningún problema en disfrutar de Mad Mansion en ese caso. Es un juego muy divertido y entretenido, que se basa en cooperar, estar atentos y usar el ingenio. Sin sustos ni sobresaltos que te dejen sin aliento... ¡¡No dudéis en visitar la Mansión de la familia Crowell!!
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_3" aria-expanded="false">
                  ¿PUEDO PASAR EN CUALQUIER MOMENTO POR EL LOCAL PARA REALIZAR LA RESERVA?¿PODRÉ IR A JUGAR SIN RESERVAR?
                </a>
              </h4>
            </div>
            <div id="panel_3" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  El horario de Mad Mansion funciona mediante reserva, por lo que normalmente sólo estaremos cerca si tenemos grupos pendientes de entrar en la Mansión.
                  Si preferís realizar la reserva con antelación en el propio local, poneros en contacto antes con nosotros mediante Email o Teléfono para quedar un día y hora en concreto y realizar allí mismo la reserva.
                  También existe la opción de ir sin reservar en el mismo día, avisándonos antes por teléfono. Si hay alguna hora libre no hay ningún problema de que el grupo entre al momento y disfrutar de esta original y divertida aventura.
                </p>
             </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_4" aria-expanded="false">
                  ¿EL VALE DE REGALO SE PUEDE COMPRAR POR LA WEB?
                </a>
              </h4>
            </div>
            <div id="panel_4" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  Si, también se puede adquirir en el propio local o podéis pedírnoslo por teléfono o Email. Siempre poniéndose en contacto con nosotros con antelación.
                  No encontraréis un regalo tan original y divertido. Recordar que cuanto menos sepan la personas invitadas, mas gracia y mas sorprendidas van a salir de Mad Mansion.
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_5" aria-expanded="false">
                  ¿SE PUEDE EXTENDER LA FECHA DE CADUCIDAD DEL VALE DE REGALO?
                </a>
              </h4>
            </div>
            <div id="panel_5" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  No, no es posible, pero la fecha de caducidad indica que el vale se debe de usar en un plazo de 2 meses máximo para realizar la reserva en Mad Mansion y ésta puede hacerse cualquier mes y día disponible dentro de nuestro calendario.
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_6" aria-expanded="false">
                  HE REALIZADO UNA RESERVA PERO NO PUEDO IR ESE MISMO DÍA. ¿QUÉ PUEDO HACER?                </a>
              </h4>
            </div>
            <div id="panel_6" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  Infórmanos por correo electrónico o llamando por teléfono, al menos 72h antes de la fecha de tu reserva, y podremos cambiarla. Si alguien tiene una reserva pagada, pero no aparece en la hora, ni nos ha avisado con 72h de antelación para cambiarla, por desgracia perderá la reserva.
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#panel_7" aria-expanded="false">
                  ¿QUÉ PASA SI NO LLEGAMOS PUNTUALES?
                </a>
              </h4>
            </div>
            <div id="panel_7" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                <p class="text">
                  La puntualidad es muy importante para que no haya retrasos con los grupos siguientes. Por lo que se ruega estar puntuales a la hora indicada. Si se llega con retraso se perderá la reserva.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

   <?php include('includes/footer.phtml');?>