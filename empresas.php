  <?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">Empresas</h2>
      <h5 class="subheadline margin40bottom">TEAM BUILDING BILBAO</h5>

      <p class="text">
        Sorprende a tus empleados de tu empresa con una innovadora actividad de Team Building en Bilbao. Tendrán que enfrentarse a interesantes retos y pruebas que deberán resolver usando su ingenio de manera creativa. La cooperación y una buena coordinación como grupo son cruciales para ello.</br>
 
        Organiza a tus empleados por equipos para que entren en Mad Mansion y compitan por ser el equipo que lo logre en menos tiempo, observar como se desenvuelven en grupo ante diversos obstáculos o simplemente ver como trabajan juntos para logra escapar.
      </p>
      <div class="box margin20top">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <img src="img/samples/room1.jpg" alt="Game 1" class="game-photo" />
          </div>
          <div class="col-xs-12 col-sm-6">
            <img src="img/samples/room2.jpg" alt="Game 2" class="game-photo" />
          </div>
        </div>
      </div>
      <p class="text">
        Contacta con nosotros para que organicemos sesiones de Team Building en Mad Mansion de la mejor manera posible, de acuerdo a vuestras necesidades. 
      </p>
      <p class="text">
        OPCIONES DE TEAMBUILDING (Más info en sección JUEGOS). </br>

        MAD MANSION 1 "Proyecto Gibeon" admite hasta 5 personas por equipo y hora.</br>
 
        MAD MANSION 2 "Una nueva especie" admite hasta 5 personas por equipo y hora.</br>
        (Nota, Hay opción de jugar 10 personas en la misma hora yendo 5 a Mad Mansion 1 y 5 a Mad Mansion 2)</br>
 
        SESIONES COMPLETAS: Si deseas reservar nuestro local 1 día entero, puedes hacerlo si tenemos disponibilidad.</br>
        Contacta con nosotros y buscaremos la mejor opción y fecha: info@madmansion.es  Telf. 622034781</br>
      </p>
    </div>
  </section>

  <?php include('includes/footer.phtml');?>