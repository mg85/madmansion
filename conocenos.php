<?php include('includes/header_common.phtml');?>

  <section class="games-layout-wrap content-wrap white">
    <div class="site-container small">
      <h2 class="section-headline">Conocenos</h2>
      <h5 class="subheadline margin40bottom">SOMOS UN EQUIPO</h5>
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <img src="img/samples/team.jpg" alt="Conocenos" class="game-photo" />
          </div>
        </div>
      </div>
      <p class="text">
       Mad Mansion es un novedoso y misterioso juego situado en el barrio de Campo Volantín - Castaños en Bilbao. Grupos de entre 2 a 5 personas deben de resolver misterios, puzzles y acertijos para poder escapar de la Mansión dentro de un tiempo limitado. El trabajo en equipo es primordial para lograrlo. Una vez se cierra la puerta de Mad Mansion ya no hay vuelta atrás.
      </p>
      <p class="text">
        ¿A qué esperáis para vivir algo nuevo y emocionante? No dudéis de que vuestra experiencia no será sino positiva.
      </p>
      <p class="text">
        Bienvenidos a ¡Mad Mansion!
      </p>
      <h2 class="section-headline margin40top margin20bottom">NOTICIAS SOBRE MAD MANSION</h2>
      <div class="row conocenos-grid">
        <div class="col-xs-12 col-sm-6 games-box">
          <div class="item">
            <div class="box">
              <img class="img-responsive" src="img/aboutus/newyorktimes.png" alt="">
            </div>
            <div class="caption">
              <h3 class="headline">MAD MANSION EN NEW YORK TIMES</h3>
              <p class="text">
                ¡Un verdadero honor y gracias por el apoyo de todos!
              
              </p>
              <a class="yellow-btn" target="_blank" href="https://www.youtube.com/watch?v=Wyv0CCE5SB0">VER VIDEO</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 games-box">
          <div class="item">
            <div class="box">
              <img class="img-responsive" src="img/aboutus/bilbofest.png" alt="">
            </div>
            <div class="caption">
              <h3 class="headline">"LA ATRACCION DE MODA"</h3>
              <p class="text">
                Los chicos de Bilbofest nos dedican la siguiente reseña:
              </p>
              <a class="yellow-btn" target="_blank" href="http://bilbofest.com/mad-mansion-la-atraccion-de-moda-en-bilbao/">LEER MÁS</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 games-box">
          <div class="item">
            <div class="box">
              <img class="img-responsive" src="img/aboutus/esvivir.png" alt="">
            </div>
            <div class="caption">
              <h3 class="headline">EN LA REVISTA DIGITAL</h3>
              <p class="text">
                "Cómo salir de una siniestra mansión y no morir en el intento":
              </p>
              <a class="yellow-btn" target="_blank" href="http://www.esvivir.com/noticia/1642/como-salir-de-una-siniestra-mansion-y-no-morir-en-el-intento#.VBl8yPl_vYj">LEER MÁS</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 games-box">
          <div class="item">
            <div class="box">
              <img class="img-responsive" src="img/aboutus/eitb.png" alt="">
            </div>
            <div class="caption">
              <h3 class="headline">EITB "MÁS DE 3 MILLONES"</h3>
              <p class="text">
                El programa "Más de 3 millones" nos han dedicado este espacio en su programa:
              </p>
              <a class="yellow-btn" target="_blank" href="http://www.eitb.eus/es/television/programas/mas-de-tres-millones/videos/detalle/3955146/video-hablamos-creador-mad-mansion/">VER VIDEO</a>
            </div>
          </div>
        </div>
      </div>
      <h2 class="section-headline margin40top margin20bottom">Opiniones</h2>
      <div id="TA_selfserveprop189" class="TA_selfserveprop">
        <ul id="AEPlfe267Z9" class="TA_links y3ER6A">
          <li id="13MmVe5" class="g5No8k9">
            <a target="_blank" href="https://www.tripadvisor.es/"><img src="https://www.tripadvisor.es/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
          </li>
        </ul>
      </div>
      <script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=189&amp;locationId=6721748&amp;lang=es&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=true&amp;border=true&amp;display_version=2"></script>

    </div>
  </section>

  <?php include('includes/footer.phtml');?>