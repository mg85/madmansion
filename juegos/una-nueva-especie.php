  <?php include('../includes/header_common.phtml');?>


  <section class="bg-headline-wrap">
    <div class="inner-wrap">
      <h1 class="section-headline white">MAD MANSION 2: UNA NUEVA ESPECIE</h1>
    </div>
  </section>

  <section class="content-wrap white">
    <div class="site-container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 margin20bottom">
          <h6 class="small-headline">Video</h6>
          <a href="//www.youtube.com/embed/qg_b2fdJZFE?autoplay=1" class="video-player-box video-link fancybox.iframe">
            <img src="../img/mad2/video.png" alt="Youtube" />
          </a>
          <h6 class="small-headline">Gallery</h6>
          <ul class="gallery-box">
            <li><a href="../img/mad2/1.png?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad2/1.png?v=1" alt="Img 1" /></a></li>
            <li><a href="../img/mad2/2.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad2/2.jpg?v=1" alt="Img 2" /></a></li>
            <li><a href="../img/mad2/3.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad2/3.jpg?v=1" alt="Img 3" /></a></li>
            <li><a href="../img/mad2/4.jpg?v=1" class="fancybox" data-fancybox-group="gallery"><img src="../img/mad2/4.jpg?v=1" alt="Img 4" /></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-6 margin20bottom">
          <h6 class="small-headline">SINOPSIS</h6>
          <p class="text">
            El Dr. Crowell os ha atrapado en su mansión y quiere llevar a cabo el "Proyecto Gibeon" con vosotros. Disponéis de 1 hora para sabotearle el plan y escapar antes de que sea demasiado tarde. Solo usando vuestro ingenio y siendo más astutos que él, podréis lograrlo. ¡Suerte!
          </p>
<div id="widget-iframe-content" data-app-url="https://www.ticketself.com/" data-event-url="https://www.ticketself.com/event/4b8d68fd?widget=1&widconf=23"> Compra aquí tus entradas</div> <script type="text/javascript" src="https://www.ticketself.com/assets/default/js/widget-iframe.js?v=7"></script>
          </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <div class="room-info">
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-timer"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">DURACION</h6>
                <p class="text big">60 mins.</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-user"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline">Participantes</h6>
                <p class="text big"> 2-5 personas</p>
              </div>
            </div>
            <div class="room-att-info">
              <div class="icon">
                <i class="pe-7s-cash"></i>
              </div>
              <div class="room-caption">
                <h6 class="small-headline"> Precio</h6>
                <p class="text big">65 &euro; (Grupo)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

   <?php include('../includes/footer.phtml');?>
